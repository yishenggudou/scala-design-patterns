DOC_DIR     = ./docs
V_PYTHON    = ./venv/bin/activate
DOC_HTML_FILE     = ./docs/_build/html/index.html

.PHONY: help

help:
	@python -c "import re;help='\n'.join([line.replace('##','\n\t') for line in open('Makefile').readlines() if re.search('^[a-zA-Z0-9\-\_]*?\:[\s\S]*?$$',line,flags=re.I)]);print help"

.DEFAULT_GOAL := help

.PHONY: build-doc

build-doc: ##build doc
	. ${V_PYTHON}; cd docs; make html

open-doc: build-doc ##build and open docs
	open ${DOC_HTML_FILE}


