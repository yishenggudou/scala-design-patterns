workflow "New workflow" {
  on = "push"
  resolves = ["GitHub Action for Slack"]
}

action "Setup Java Action" {
  uses = "actions/setup-java@40205d2e1686857b375bb134525a667366afd2d3"
  secrets = ["GITHUB_TOKEN"]
  runs = " mvn test"
}

action "GitHub Action for Maven" {
  uses = "LucaFeger/action-maven-cli@765e218a50f02a12a7596dc9e7321fc385888a27"
  needs = ["Setup Java Action"]
  secrets = ["GITHUB_TOKEN"]
  args = "clean install test"
}

action "GitHub Action for Slack" {
  uses = "Ilshidur/action-slack@4059a1a6baf83f0df895638cfa80ff61b9fa19c7"
  needs = ["GitHub Action for Maven"]
  secrets = ["GITHUB_TOKEN"]
}
